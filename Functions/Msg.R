#' Mensagens
#'
#' @return
#' @export
#'
#' @examples
Msg <- function(...){
  
  # Exibe mensagem
  cat(
    str_c(
      "\n",
      ...,
      "\n"
    )
  )
  
  # Funcao sem retorno
  invisible()
}
####
## Fim
#